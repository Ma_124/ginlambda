package ginlambda

import (
	"fmt"
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	ginadapter "github.com/awslabs/aws-lambda-go-api-proxy/gin"
	"github.com/gin-gonic/gin"
	"net/http"
	"os"
	"sync"
)

// The About struct contains information about your program.
type About struct {
	Name    string `json:"name"`
	Version string `json:"version"`

	EnvPrefix string `json:"env_prefix,omitempty"`
}

// Start parses the commandline options with github.com/spf13/pflag and calls StartWithOptions.
//
// If you want a really small binary specify the builtinflag build tag to use the flag package instead.
func Start(r *gin.Engine, cfg *About) (err error) {
	// returns flag, pflag or noop flagset
	f := newFlagger(cfg.Name)
	opts := &Options{}
	BindFlags(f, opts)

	err = f.Parse(os.Args[1:])
	if err != nil {
		return
	}

	StartWithOptions(r, cfg, opts)
	return
}

// contains whether a server was launched
// only non nil during testing
var launchTest map[string]bool

// StartWithOptions starts the server(s) according the rules in the documentation of the Options type.
func StartWithOptions(r *gin.Engine, cfg *About, opts *Options) {
	if opts.Help {
		printHelp(cfg)
		return
	}
	if opts.Version {
		printVersion(cfg)
		return
	}

	opts.LoadEnv(cfg.EnvPrefix)

	if opts.HttpAddr == "" && opts.HttpsAddr == "" && !opts.Lambda {
		fmt.Fprint(writer, "ginlambda: no server launched\n\n")
		printHelp(cfg)
		return
	}

	var wg sync.WaitGroup
	var errh = func(h string, err error) {
		fmt.Fprintf(writer, "ginlambda: %s failed with: %q\n", h, err)
	}

	if opts.HttpAddr != "" {
		if launchTest == nil {
			wg.Add(1)
			go func() {
				defer wg.Done()

				err := http.ListenAndServe(opts.HttpAddr, r)
				if err != nil {
					errh("http", err)
				}
			}()
		} else {
			launchTest["http"] = true
		}
	}

	if opts.HttpsAddr != "" {
		if launchTest == nil {
			wg.Add(1)
			go func() {
				defer wg.Done()

				err := http.ListenAndServeTLS(opts.HttpsAddr, opts.TLSCertPath, opts.TLSKeyPath, r)
				if err != nil {
					errh("https", err)
				}
			}()
		} else {
			launchTest["https"] = true
		}
	}

	if opts.Lambda {
		if launchTest == nil {
			wg.Add(1)
			go func() {
				defer wg.Done()

				// TODO prevent os.Exit
				adapter := ginadapter.New(r)
				lambda.Start(func(req events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
					return adapter.Proxy(req)
				})
			}()
		} else {
			launchTest["lambda"] = true
		}
	}

	wg.Wait()
}

func printVersion(cfg *About) {
	fmt.Fprintf(writer, "%s v%s\n", cfg.Name, cfg.Version)
}
func printHelp(cfg *About) {
	fmt.Fprint(writer, helpText(cfg))
}
