package ginlambda

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"os"
	"strings"
	"testing"
)

var exampleCfg = &About{
	Name:      "example",
	Version:   "1.2.3",
	EnvPrefix: "TEST_NOPT_",
}

func TestStart(t *testing.T) {
	t.Run("http", func(t *testing.T) {
		launchTest = make(map[string]bool)
		os.Args = []string{"", "--http", "a"}
		assert.NoError(t, Start(nil, exampleCfg))
		assert.Equal(t, ltStruct{
			Http: true,
		}, lts())
	})

	t.Run("err", func(t *testing.T) {
		launchTest = make(map[string]bool)
		os.Args = []string{"", "--unk", "a"}
		assert.Error(t, Start(nil, exampleCfg))
		assert.Equal(t, ltStruct{}, lts())
	})

	t.Run("help", func(t *testing.T) {
		assertStdout(t, "")
		launchTest = make(map[string]bool)
		os.Args = []string{"", "--help"}
		assert.NoError(t, Start(nil, exampleCfg))
		assert.Equal(t, ltStruct{}, lts())
		assertStdout(t, helpText(exampleCfg))
	})

	t.Run("no server", func(t *testing.T) {
		assertStdout(t, "")
		launchTest = make(map[string]bool)
		os.Args = []string{""}
		assert.NoError(t, Start(nil, exampleCfg))
		assert.Equal(t, ltStruct{}, lts())
		assertStdout(t, "ginlambda: no server launched\n\n" + helpText(exampleCfg))
	})

	t.Run("version", func(t *testing.T) {
		assertStdout(t, "")
		launchTest = make(map[string]bool)
		os.Args = []string{"", "--version"}
		assert.NoError(t, Start(nil, exampleCfg))
		assert.Equal(t, ltStruct{}, lts())
		assertStdout(t, fmt.Sprintf("%s v%s\n", exampleCfg.Name, exampleCfg.Version))
	})

	t.Run("invalid lambda env", func(t *testing.T) {
		assertStdout(t, "")
		launchTest = make(map[string]bool)
		os.Args = []string{"", "--http", "a"}
		assert.NoError(t, os.Setenv("TEST_OPT_INV_LAMB_LAMBDA", "a"))
		assert.NoError(t, Start(nil, &About{
			Name:      exampleCfg.Name,
			Version:   exampleCfg.Version,
			EnvPrefix: "TEST_OPT_INV_LAMB_",
		}))
		assert.Equal(t, ltStruct{
			Http: true,
		}, lts())
		assertStdout(t, `ginlambda: invalid $TEST_OPT_INV_LAMB_LAMBDA: "a" (err: "strconv.ParseBool: parsing \"a\": invalid syntax") (default used: false)`)
	})

	launchTest = make(map[string]bool)
	os.Args = []string{""}
}

func TestMain(m *testing.M) {
	writer = &strings.Builder{}

	os.Exit(m.Run())
}

func assertStdout(t *testing.T, expected string) bool {
	s := writer.(*strings.Builder).String()
	writer.(*strings.Builder).Reset()
	return assert.Equal(t, expected, s)
}

type ltStruct struct {
	Http   bool
	Https  bool
	Lambda bool
}

func lts() ltStruct {
	return ltStruct{
		Http:   launchTest["http"],
		Https:  launchTest["https"],
		Lambda: launchTest["lambda"],
	}
}
