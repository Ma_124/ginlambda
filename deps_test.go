package ginlambda

import (
	"go/build"
	"io/ioutil"
	"os"
	"os/exec"
	"strings"
	"testing"
)

func loadDeps(t *testing.T, p string) map[string]struct{} {
	_, err := exec.LookPath("go")
	if err != nil {
		t.Fatal("the go executable is not in your path")
	}
	args := []string{"list", "-f", `{{ range .Deps }}{{ . }}{{ "\x00" }}{{ end }}`, p}
	cmdtxt := "go '" + strings.Join(args, "', '") + "'"
	cmd := exec.Command("go", args...)
	cmd.Stderr = os.Stderr
	pipe, err := cmd.StdoutPipe()
	if err != nil {
		t.Fatalf("could not pipe stdout of command %s (err: %q)", cmdtxt, err)
	}

	err = cmd.Start()
	if err != nil {
		t.Fatalf("could not start command %s (err: %q)", cmdtxt, err)
	}

	data, err := ioutil.ReadAll(pipe)
	if err != nil {
		t.Fatalf("could not read stdout of command %s (err: %q)", cmdtxt, err)
	}

	err = cmd.Wait()
	if err != nil {
		t.Fatalf("command %s failed (err: %q)", cmdtxt, err)
	}

	deps := strings.Split(string(data), "\x00")
	depm := make(map[string]struct{}, len(deps))
	for _, dep := range deps {
		depm[dep] = struct{}{}
	}

	return depm
}

func TestDepsNew(t *testing.T) {
	lambdaPkg, err := build.Import("gitlab.com/Ma_124/ginlambda", "", 0)
	if err != nil {
		t.Fatal("could not import gitlab.com/Ma_124/ginlambda")
	}
	ginDeps := loadDeps(t, "github.com/gin-gonic/gin")

	for _, imp := range lambdaPkg.Imports {
		_, ok := ginDeps[imp]
		if !ok {
			switch imp {
			case "github.com/gin-gonic/gin":
			case "flag", "github.com/spf13/pflag":
			case "github.com/aws/aws-lambda-go/lambda":
			case "github.com/awslabs/aws-lambda-go-api-proxy/gin":
			default:
				t.Fatalf("invalid dependency %q", imp)
			}
		}
	}
}
