// +build builtinflag

package ginlambda

import (
	"flag"
	"os"
)

func newFlagger(n string) Flagger {
	return flag.NewFlagSet(os.Args[0], flag.ContinueOnError)
}
