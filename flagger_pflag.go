// +build !builtinflag

package ginlambda

import (
	"github.com/spf13/pflag"
)

func newFlagger(n string) Flagger {
	return pflag.NewFlagSet(n, pflag.ContinueOnError)
}
