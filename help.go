package ginlambda

const usageText = `Options:

  -h, --help       Show help and exit
  -v, --version    Show version and exit
      --http       Start an HTTP server with the given address ($HTTP)
      --https      Start an HTTPS server with the given address ($HTTPS)
      --tls-key    The TLS key to use for https ($TLS_KEY)
      --tls-cert   The TLS cert to use for https ($TLS_CERT)
      --lambda     Start a Lambda server (default if the _LAMBDA_SERVER_PORT env variable is set)
`

func helpText(cfg *About) string {
	return cfg.Name + "\n\n" + usageText
}
