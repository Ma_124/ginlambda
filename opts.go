package ginlambda

import (
	"fmt"
	"io"
	"os"
	"strconv"
)

// Flagger is an interface satisfied by flag.FlagSet and other implementations
// of flags.
type Flagger interface {
	Parse([]string) error
	StringVar(p *string, name string, value string, usage string)
	IntVar(p *int, name string, value int, usage string)
	BoolVar(p *bool, name string, value bool, usage string)
	Float64Var(p *float64, name string, value float64, usage string)
}

type pflagger interface {
	BoolVarP(p *bool, name string, shorthand string, value bool, usage string)
}

var writer io.Writer = os.Stdout

// The Options to start the servers.
//
// See the struct tags for information about the individual options.
//
// It provides struct tags for encoding/json and github.com/mkideal/cli.
// If you want to use this with something that implements Flagger use BindFlags(flagSet, opts) (also see Start()).
type Options struct {
	Help    bool `cli:"!help,h"    json:"-" usage:"Show help and exit"`
	Version bool `cli:"!version,v" json:"-" usage:"Show version and exit"`

	HttpAddr    string `cli:"http"     json:"http_addr,omitempty"     usage:"Start an HTTP server with the given address ($HTTP)"`
	HttpsAddr   string `cli:"https"    json:"https_addr,omitempty"    usage:"Start an HTTPS server with the given address ($HTTPS)"`
	TLSKeyPath  string `cli:"tls-key"  json:"tls_key_path,omitempty"  usage:"The TLS key to use for https ($TLS_KEY)"`
	TLSCertPath string `cli:"tls-cert" json:"tls_cert_path,omitempty" usage:"The TLS cert to use for https ($TLS_CERT)"`

	Lambda bool `cli:"lambda" json:"lambda,omitempty" usage:"Start a Lambda server (default if the _LAMBDA_SERVER_PORT env variable is set)"`
}

func (opts *Options) AutoHelp() bool {
	return opts.Help
}

// LoadEnv loads env variables described in the documentation of the Options type.
func (opts *Options) LoadEnv(env string) {
	if opts.HttpAddr == "" {
		opts.HttpAddr = os.Getenv(env + "HTTP")
	}
	if opts.HttpsAddr == "" {
		opts.HttpsAddr = os.Getenv(env + "HTTPS")
	}
	if opts.TLSKeyPath == "" {
		opts.TLSKeyPath = os.Getenv(env + "TLS_KEY")
	}
	if opts.TLSCertPath == "" {
		opts.TLSCertPath = os.Getenv(env + "TLS_CERT")
	}

	if !opts.Lambda {
		lambdaEnv := os.Getenv(env + "LAMBDA")
		if lambdaEnv != "" {
			var err error
			opts.Lambda, err = strconv.ParseBool(lambdaEnv)
			if err != nil {
				fmt.Fprintf(writer, "ginlambda: invalid $%sLAMBDA: %q (err: %q) (default used: %v)", env, lambdaEnv, err, opts.Lambda)
			}
		} else {
			opts.Lambda = opts.HttpAddr == "" && opts.HttpsAddr == "" && os.Getenv("_LAMBDA_SERVER_PORT") != ""
		}
	}
}

// BindFlags binds the opts to a Flagger (e.g. flag.FlagSet).
func BindFlags(f Flagger, opts *Options) {
	if pf, ok := f.(pflagger); ok {
		pf.BoolVarP(&opts.Version, "version", "v", false, "Show version and exit")
		pf.BoolVarP(&opts.Help, "help", "h", false, "Show help and exit")
	} else {
		f.BoolVar(&opts.Version, "version", false, "Show version and exit")
		f.BoolVar(&opts.Help, "help", false, "Show help and exit")
	}

	f.StringVar(&opts.HttpAddr, "http", "", "Start an HTTP server with the given address ($HTTP)")
	f.StringVar(&opts.HttpAddr, "https", "", "Start an HTTPS server with the given address ($HTTPS)")
	f.StringVar(&opts.TLSKeyPath, "tls-key", "", "The TLS key to use for https ($TLS_KEY)")
	f.StringVar(&opts.TLSCertPath, "tls-cert", "", "The TLS cert to use for https ($TLS_CERT)")

	f.BoolVar(&opts.Lambda, "lambda", false, "Start a Lambda server (default if the _LAMBDA_SERVER_PORT env variable is set)")
}
