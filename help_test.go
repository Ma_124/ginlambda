package ginlambda

import (
	"flag"
	"github.com/iancoleman/strcase"
	"github.com/mkideal/cli"
	"github.com/stretchr/testify/assert"
	"reflect"
	"testing"
)

func TestHelpText(t *testing.T) {
	opts := &Options{}

	t.Run("github.com/mkideal/cli", func(t *testing.T) {
		cli.RunWithArgs(opts, []string{""}, func(ctx *cli.Context) error {
			// test: usageText constant is up-to-date
			assert.Equal(t, usageText, ctx.Usage())
			return nil
		})
	})
	t.Run("flag", func(t *testing.T) {
		fs := flag.NewFlagSet("ginlambda", flag.ContinueOnError)
		BindFlags(fs, opts)

		optsT := reflect.TypeOf(*opts)

		// map[name]desc
		flags := make(map[string]string, optsT.NumField())

		for i := 0; i < optsT.NumField(); i++ {
			f := optsT.Field(i)
			switch f.Name {
			case "Help", "Version":
				// test: don't expose Help and Version in JSON for now
				assert.Equal(t, "-", f.Tag.Get("json"), f.Name)
			default:
				// test: JSON tag should be snake case name and omitempty
				assert.Equal(t, strcase.ToSnake(f.Name) + ",omitempty", f.Tag.Get("json"), f.Name)
			}
			flags[f.Tag.Get("cli")] = f.Tag.Get("usage")
		}

		fs.VisitAll(func(f *flag.Flag) {
			// help and version are force flags with a short alias in cli
			if f.Name == "help" {
				f.Name = "!help,h"
			} else if f.Name == "version" {
				f.Name = "!version,v"
			}

			// test: cli desc and flag desc match
			assert.Equal(t, flags[f.Name], f.Usage, f.Name)
		})
	})
}
