package ginlambda

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
)

func TestOptions_LoadEnv(t *testing.T) {
	t.Run("explicit env", func(t *testing.T) {
		opts := &Options{}
		assert.NoError(t, os.Setenv("TEST_OPT_HTTP", "ENV_HTTP"))
		assert.NoError(t, os.Setenv("TEST_OPT_HTTPS", "ENV_HTTPS"))
		assert.NoError(t, os.Setenv("TEST_OPT_TLS_KEY", "ENV_TLS_KEY"))
		assert.NoError(t, os.Setenv("TEST_OPT_TLS_CERT", "ENV_TLS_CERT"))
		assert.NoError(t, os.Setenv("TEST_OPT_LAMBDA", "true"))

		opts.LoadEnv("TEST_OPT_")
		assert.Equal(t, &Options{
			HttpAddr:    "ENV_HTTP",
			HttpsAddr:   "ENV_HTTPS",
			TLSKeyPath:  "ENV_TLS_KEY",
			TLSCertPath: "ENV_TLS_CERT",
			Lambda:      true,
		}, opts)
	})
	t.Run("_LAMBDA_SERVER_PORT ", func(t *testing.T) {
		opts := &Options{}
		defer func() {
			assert.NoError(t, os.Unsetenv("_LAMBDA_SERVER_PORT"))
		}()
		assert.NoError(t, os.Setenv("_LAMBDA_SERVER_PORT", "8347"))
		opts.LoadEnv("TEST_NOPT_")
		assert.Equal(t, &Options{
			Lambda:      true,
		}, opts)
	})
}
